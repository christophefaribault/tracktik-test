<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

Items
<ul>
    <?php foreach ($items->getSortedItems() as $item): ?>
        <li>
            <?=ucfirst($item->getType())?> : <?=$item->getPrice()?>$
            <?php if(count($item->getExtras()) > 0): ?>
                <ul>
                    <?php foreach ($item->getExtras() as $extra): ?>
                        <li>
                            <?=ucfirst($extra->getType())?><?=$extra->getWired() ? ' (wired)' : ''?>:
                            <?=$extra->getPrice() > 0 ? $extra->getPrice().'$' : 'included'?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>

<p>Total: <?=$total?>$</p>

<p>As for the friend's question, the answer is <?=$consolePrice?>.</p>
