<?php

/**
 * Class TT_Controller
 */
class TT_Controller extends ElectronicItem {

    /**
     * TT_Controller constructor.
     */
    public function __construct() {
        $this->setType(self::ELECTRONIC_ITEM_CONTROLLER);
    }

    /**
     * @return int
     */
    function maxExtras() {
        return true;
    }
}