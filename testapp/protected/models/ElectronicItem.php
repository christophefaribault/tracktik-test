<?php

/**
 * Class ElectronicItem
 */
class ElectronicItem {

    /**
     * @var float
     */
    public $price;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    public $wired;

    /**
     * @var array
     */
    protected $extras = [];

    const ELECTRONIC_ITEM_TELEVISION = 'television';
    const ELECTRONIC_ITEM_CONSOLE = 'console';
    const ELECTRONIC_ITEM_MICROWAVE = 'microwave';
    const ELECTRONIC_ITEM_CONTROLLER = 'controller';

    /**
     * @var array
     */
    public static $types = array(
        self::ELECTRONIC_ITEM_CONSOLE,
        self::ELECTRONIC_ITEM_MICROWAVE,
        self::ELECTRONIC_ITEM_TELEVISION,
        self::ELECTRONIC_ITEM_CONTROLLER
    );

    /**
     * @param TT_Controller $extra
     */
    function addExtra($extra) {
        if(!$this->maxExtras()) {
            array_push($this->extras, $extra);
        }
    }

    /**
     * @return array
     */
    function getExtras() {
        return $this->extras;
    }

    /**
     * @return float
     */
    function getPrice() {
        return $this->price;
    }

    /**
     * @return string
     */
    function getType() {
        return $this->type;
    }

    /**
     * @return bool
     */
    function getWired() {
        return $this->wired;
    }

    /**
     * @param float $price
     */
    function setPrice($price) {
        $this->price = $price;
    }

    /**
     * @param string $type
     */
    function setType($type) {
        $this->type = $type;
    }

    /**
     * @param bool $wired
     */
    function setWired($wired) {
        $this->wired = $wired;
    }
}
