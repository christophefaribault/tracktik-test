<?php

/**
 * Class TT_Television
 */
class TT_Television extends ElectronicItem {

    /**
     * TT_Television constructor.
     */
    public function __construct() {
        $this->setType(self::ELECTRONIC_ITEM_TELEVISION);
    }

    /**
     * @return int
     */
    function maxExtras() {
        return false;
    }
}