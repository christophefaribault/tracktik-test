<?php

/**
 * Class TT_Microwave
 */
class TT_Microwave extends ElectronicItem {

    /**
     * TT_Microwave constructor.
     */
    public function __construct() {
        $this->setType(self::ELECTRONIC_ITEM_MICROWAVE);
    }

    /**
     * @return int
     */
    function maxExtras() {
        return true;
    }
}