<?php

class ElectronicItems {

    /**
     * @var array
     */
    private $items = array();

    /**
     * ElectronicItems constructor.
     * @param array $items
     */
    public function __construct(array $items) {

        $this->items = $items;
    }

    /**
     * Returns the items depending on the sorting type requested
     *
     * @return array
     */
    public function getSortedItems() {

        $sorted = array();
        foreach ( $this->items as $item ) {

            $sorted[($item->price * 100)] = $item;
        }

        ksort($sorted, SORT_NUMERIC);

        return $sorted;
    }

    /**
     * Get list of items by specified type
     *
     * @param string $type
     * @return array|bool
     */
    public function getItemsByType( $type ) {

        if ( in_array($type, ElectronicItem::$types) ) {

            $callback = function ($item) use ($type) {
                return $item->getType() == $type;
            };

            return array_filter($this->items, $callback);
        }

        return false;
    }
}
