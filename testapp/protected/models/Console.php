<?php

/**
 * Class TT_Console
 */
class TT_Console extends ElectronicItem {

    /**
     * TT_Console constructor.
     */
    public function __construct() {
        $this->setType(self::ELECTRONIC_ITEM_CONSOLE);
    }

    /**
     * @return int
     */
    function maxExtras() {
        return count($this->extras) >= 4;
    }
}