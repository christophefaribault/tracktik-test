<?php

include __DIR__ . '/../models/ElectronicItems.php';
include __DIR__ . '/../models/ElectronicItem.php';
include __DIR__ . '/../models/Console.php';
include __DIR__ . '/../models/Television.php';
include __DIR__ . '/../models/Microwave.php';
include __DIR__ . '/../models/Controller.php';

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // Create console
        $console = new TT_Console();
        $console->setPrice(40);

        $console_controller_1 = new TT_Controller();
        $console_controller_2 = clone $console_controller_1;
        $console_controller_3 = clone $console_controller_1;
        $console_controller_4 = clone $console_controller_1;
        $console_controller_5 = clone $console_controller_1;

        $console_controller_1->setPrice(1);
        $console_controller_2->setPrice(1);

        $console_controller_3->setWired(true);
        $console_controller_4->setWired(true);

        $console->addExtra($console_controller_1);
        $console->addExtra($console_controller_2);
        $console->addExtra($console_controller_3);
        $console->addExtra($console_controller_4);

        // This extra should not be shown because it is over the limit
        $console->addExtra($console_controller_5);

        // Create television 1
        $television1 = new TT_Television();
        $television1->setPrice(200);

        $television1_controller_1 = new TT_Controller();
        $television1_controller_2 = clone $television1_controller_1;

        $television1->addExtra($television1_controller_1);
        $television1->addExtra($television1_controller_2);

        // Create television 2
        $television2 = new TT_Television();
        $television2->setPrice(300);

        $television2_controller_1 = new TT_Controller();

        $television2->addExtra($television2_controller_1);

        // Create microwave
        $microwave = new TT_Microwave();
        $microwave->setPrice(100);

        $microwave_controller_1 = new TT_Controller();

        // This extra should not be shown because microwaves can not have extras
        $microwave->addExtra($microwave_controller_1);

        // Create items list
        $items = new ElectronicItems([
            $console,
            $television1,
            $television2,
            $microwave
        ]);

        // Get items total
        $total = 0;
        foreach($items->getSortedItems() as $item) {
            $total += $item->getPrice();
            if($item->maxExtras() <> 0) {
                foreach ($item->getExtras() as $extra) {
                    $total += $extra->getPrice();
                }
            }
        }

        // Get console iterm
        $consoleItems = $items->getItemsByType(ElectronicItem::ELECTRONIC_ITEM_CONSOLE);

        // Get console price
        $consolePrice = 0;
        foreach($consoleItems as $item) {
            $consolePrice += $item->getPrice();
            if($item->maxExtras() <> 0) {
                foreach ($item->getExtras() as $extra) {
                    $consolePrice += $extra->getPrice();
                }
            }
        }

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', [
            'items' => $items,
            'total' => $total,
            'consolePrice' => $consolePrice,
        ]);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}